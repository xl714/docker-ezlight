#!/bin/bash

# USE:
# /bin/bash /projects/docker-ezlight/php-fpm/init-symfony-project.sh projectStringId serverName
# EXAMPLE:
# /bin/bash /projects/docker-ezlight/php-fpm/init-symfony-project.sh drivouycar www.drivouycar.dtd

set -x

project=$1
server_name=$2
sites_file_name="/projects/docker-ezlight/php-fpm/list-symfony-sites.sh"

###################
#########
#####
###
##
echo "##### 1 >>> Add to sites list"

source $sites_file_name

if test ${sfsites[$project]+isset}; then
    echo "Already exists in sites list"
else
    sed -i -e "s/);/    [${project}]=\"${server_name}\"\n);/g" $sites_file_name
    source $sites_file_name
    if test ${sfsites[$project]+isset}; then
        echo "Added successfully to sites list"
    else
        echo "Error - Adding to sites list failed"
        exit 1
    fi;
fi;

###################
#########
#####
###
##
echo "##### 2 >>> create NGINX conf and put it in NGINX conf directory"

nginx_conf_file="/projects/docker-ezlight/nginx/etc-nginx-sites-enabled/${project}.conf"

if [ ! -f $nginx_conf_file ]; then
    echo "Create NGINX conf and put it in NGINX conf directory"
    cp -f /projects/docker-ezlight/nginx/vhost-site-template-symfony.conf $nginx_conf_file
fi

sed -i -e "s/\${PROJECT}/${project}/g" $nginx_conf_file
sed -i -e "s/\${SERVER_NAME}/${server_name}/g" $nginx_conf_file


###################
#########
#####
###
##
echo "##### 3 >>> Create symfony 4 project"

project_dir=/projects/$project

symfony_dir=$project_dir/symfony

rm -rf $project_dir

if [ -d $project_dir ]; then
    echo "Info: $project_dir already exists"
else
#fi
    mkdir $project_dir
    cd $project_dir
    echo "Create symfony 4 project"
    composer create-project symfony/skeleton symfony
    chown -R www-data:www-data $project_dir
    chmod -R 777 $project_dir
fi

###################
#########
#####
###
##
echo "##### 4 >>> add packages symfony/orm-pack maker-bundle form translation symfony/asset  eeet !! encore !! avec yarn watch pour compiler les js/css àla volée "

cd $symfony_dir
composer require --dev symfony/orm-pack maker-bundle form translation symfony/asset symfony/twig-bundle symfony/debug symfony/profiler-pack

# composer require --dev annotations ?
#composer require --dev validator
#composer req --dev orm-fixtures
#composer require fzaninotto/faker --dev
#composer require symfony/security-bundle

###################
#########
#####
###
##
echo "##### 5 >>> add encore yarn jquery sass-loader node-sass bootstrap popper.js"

composer require --dev encore
yarn install
yarn encore dev

yarn add jquery --dev
# yarn add -P webpack --dev # to avoid warning " > sass-loader@7.1.0" has unmet peer dependency "webpack@^3.0.0 || ^4.0.0".
yarn add sass-loader node-sass --dev
yarn add bootstrap popper.js --dev

nohup yarn encore dev --watch &

###################
#########
#####
###
##
echo "##### 6 >>> Add default files"

cp -rf /projects/docker-ezlight/php-fpm/files/symfony/* $symfony_dir
chown -R www-data:www-data $project_dir
chmod -R 777 $project_dir

sed -i -e "s/PROJECT/$project/g" $symfony_dir/templates/base.html.twig
sed -i -e "s/PROJECT/$project/g" $symfony_dir/templates/parts/header.twig.html
sed -i -e "s/PROJECT/$project/g" $symfony_dir/templates/home/index.html.twig


###################
#########
#####
###
##
echo "##### 7 >>> add db correct url"

symfony_env_file=$symfony_dir/.env
symfony_env_local_file=$symfony_dir/.env.local

cp -f $symfony_env_file $symfony_env_local_file

#DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
#DATABASE_URL=mysql://admin:pass@sf4_db:3306/project_name

sed -i -e "s/db_user/admin/g" $symfony_env_local_file
sed -i -e "s/db_password/pass/g" $symfony_env_local_file
sed -i -e "s/127.0.0.1/ezlight-mariadb/g" $symfony_env_local_file
sed -i -e "s/db_name/$project/g" $symfony_env_local_file




echo "now create bitbucket project then do:"
echo "git init"
echo "git remote add origin git@bitbucket.org:dtdmondadori/$project.git"
echo "git config core.fileMode false"
echo "git add ."
echo 'git commit -m "commit initial"'
echo 'git push origin master'

#git clone git@bitbucket.org:dtdmondadori/sites-monitoring.git

#
