# DOCUMENTATION GIT DTD
==============


## sous `/projects/repo-standard.dtd`

### mettre a jour sa branche de dev

```bash
cd /projects/repo-standard.dtd

# si premiere fois

git config --global user.name "FIRST_NAME LAST_NAME"
git config --global user.email "MY_NAME@example.com"
git config core.fileMode false;

# les autres fois, très régulièrement

git fetch origin;
git checkout dev;
git pull --rebase origin dev;
```

### créer sa branche pour développer sa fonctionnalité

```bash
cd /projects/repo-standard.dtd


# mettre a jour sa dev

git checkout dev;
git pull --rebase origin dev;

# créer la nouvelle branche

git checkout -b new-branch
```

### commiter sur sa branche

```bash

cd /projects/repo-standard.dtd

git checkout new-branch

# !!! IMPORTANT !!! si on travaille sur une branche ou d'autres travaillent
git pull --rebase origin new-branch;

### !!! IMPORTANT !!! ne surtout pas faire git add .
### et ne pas faire de git add de submodule
### même si en faisant un git status on a ça c'est normal : (liste des submodules)
# modified:   application/extension/adminmf (new commits)
# modified:   application/extension/bibamagazine (new commits)
# modified:   application/extension/diapasonmag (new commits)
# modified:   application/extension/doveopenid (new commits, modified content)
# modified:   application/extension/elleadore (new commits)
# modified:   application/extension/ezless (new commits, modified content)
# modified:   application/extension/grazia (new commits, untracked content)
# modified:   application/extension/grilletv (new commits, modified content)
# modified:   application/extension/gtv (new commits, modified content)
# modified:   application/extension/guerreshistoirescienceetvie (new commits)
# modified:   application/extension/juniorscienceetvie (new commits)
# modified:   application/extension/lesveilleesdeschaumieres (new commits)
# modified:   application/extension/mfbridge (new commits)
# modified:   application/extension/mfxmlexport (new commits, modified content, untracked content)
# modified:   application/extension/mfxmlinstaller (new commits)
# modified:   application/extension/modesettravaux (new commits, untracked content)
# modified:   application/extension/pleinevie (new commits)
# modified:   application/extension/reponsesphoto (new commits)
# modified:   application/extension/scienceetvie (new commits, untracked content)
# modified:   application/extension/ssn (new commits, modified content)
# modified:   application/extension/telepoche (new commits)
# modified:   application/extension/telestar (new commits, untracked content)
# modified:   application/extension/topsante (new commits)
# modified:   application/extension/vital (new commits)

### !!! IMPORTANT !!! il ne faut pas non faire git add du fichier site.ini qu'on a modifié pour accéder à sa base de données locale

# mais on peut faire :

git add my-modified-file.txt

git add my-new-file.txt

git commit -m "[STD]ma fonctionnalité - https://trello.com/c/qOL77YBr" # pour le lien du trello, s'arreter à l'id, sinon c'est trop long

git push origin new-branch

# on utilise des raccourcis pour les sites [STD] = standard (utilisable sur tous les sites)
# [cs]="closermag"
# [gz]="grazia"
# [ts]="topsante"
# [tls]="telestar"
# [pv]="pleinevie"
# [met]="modesettravaux"
# [sev]="scienceetvie"
# [svj]="juniorscienceetvie"
# [geh]="guerreshistoirescienceetvie"
# [dps]="diapasonmag"
# [ea]="elleadore"
# [inf]="influentes"
# [rp]="reponsesphoto"
# [vtl]="vital"
# [ssn]="ssn"
# [vdc]="lesveilleesdeschaumieres"
# [gtv]="gtv"

```

### mettre la new-branch sur la branche de preprod pour envoyer à tester

```bash

cd /projects/repo-standard.dtd

git checkout new-branch

# !!! IMPORTANT !!! si on travaille sur une branche ou d'autres travaillent
git pull --rebase origin new-branch;

git checkout preprod

# !!! IMPORTANT !!!
git pull --rebase origin preprod

git rebase new-branch

git pull --rebase origin preprod # oui, il faut le faire une autre fois ce n'est pas une erreur

git push origin preprod


# s'il y a des soucis, NE SURTOUT PAS FAIRE
# git push origin preprod -f
# ni
# git push origin preprod --force

# car il faut régler ses problèmes en local (HEAD) avant de mettre son désordre en distant (origin)

```

### une fois testé et approuvé et que !!! IMPORTANT !!! ça peut aller en PROD !!! IMPORTANT !!!
### mettre sur la branche de dev

```bash

cd /projects/repo-standard.dtd

git checkout new-branch

# !!! IMPORTANT !!! si on travaille sur une branche ou d'autres travaillent
git pull --rebase origin new-branch;


git checkout dev

# !!! TRES IMPORTANT !!!
git pull --rebase origin dev

git rebase new-branch

git pull --rebase origin dev # oui, une 2ème fois

# si pas de soucis :
git push origin dev

# s'il y a des soucis, NE SURTOUT PAS FAIRE
# git push origin dev -f
# ni
# git push origin dev --force

# car il faut régler ses problèmes en local (HEAD) avant de mettre son désordre en distant (origin)

```

J'ai marqué * !!! IMPORTANT !!! * parfois mais en fait tout est important pour que ça se passe bien
