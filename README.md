# Installation des projects à partir de docker
- Etre sur un environnement linux
- Avoir un compte bitbucket (https://bitbucket.org/) et s'assurer d'avoir accès aux repos bitbucket par exp (https://bitbucket.org/dtdmondadori/standard.digimondo.net/src/master/) demander une invitation à Remi
- Création d'une clé ssh

     https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent

     à la fin vous devez avoir ces 2 fichiers :

          - /home/(your username)/.ssh/id_rsa.pub
          - /home/(your username)/.ssh/id_rsa


- Se logger sur https://bitbucket.org/ cliquer sur le dernier bouton en bas à gauche (your profil and setting)
    cliquez sur  Bitbucket setting > "Security > ssh keys" et rentrez votre clé public  qui se trouve dans `/home/(your username)/.ssh/id_rsa.pub` generée précedement.


- Intaller git, curl sur sa machine

```bash
apt update;
apt install git curl; 
```

- Intaller docker, docker-compose

Pour installer docker cf : https://docs.docker.com/install/linux/docker-ce/ubuntu/

Pour docker-compose cf : https://docs.docker.com/compose/install/ (ne PAS faire : apt install docker-compose qui installe une version trop ancienne)

Exemple de versions qui fonctionnent : 

```bash
docker -v
Docker version 19.03.5
docker-compose -v
docker-compose version 1.23.2
```

- Création d'un nouveau dossier "projects" à la racine / 

```bash
mkdir /projects;
```

- Recupération du repo git docker

```bash
cd /projects;
git clone https://bitbucket.org/xl714/docker-ezlight.git;
cd docker-ezlight/php-fpm;
./setup-repos-git.sh; 
# (Repondre yes à la question posée)
```

## vérifier des branches git 

```bash
cd /projects/repo-standard.dtd

git fetch origin;
git checkout dev;
git pull --rebase origin dev;
git config core.fileMode false;

git submodule foreach git fetch origin;
git submodule foreach git checkout dev;
git submodule foreach git config core.fileMode false;
git submodule foreach git pull --rebase origin dev;

chmod -R 777 .;
```

 - Remplir le fichier hosts dans /etc/hosts

```bash
127.0.0.1    www.cs-dev.dtd admin.cs-dev.dtd m.cs-dev.dtd amp.cs-dev.dtd file1.cs-dev.dtd aliceadsl.cs-dev.dtd
127.0.0.1    www.gz-dev.dtd admin.gz-dev.dtd m.gz-dev.dtd file1.gz-dev.dtd amp.gz-dev.dtd bla.bla.gz-dev.dtd
127.0.0.1    www.ts-dev.dtd admin.ts-dev.dtd m.ts-dev.dtd file1.ts-dev.dtt
127.0.0.1    www.tls-dev.dtd admin.tls-dev.dtd m.tls-dev.dtd amp.tls-dev.dtd file1.tls-dev.dtd
127.0.0.1    www.gtv-dev.dtd admin.gtv-dev.dtd file1.gtv-dev.dtd
127.0.0.1    www.pv-dev.dtd admin.pv-dev.dtd m.pv-dev.dtd amp.pv-dev.dtd file1.pv-dev.dtd
127.0.0.1    www.met-dev.dtd admin.met-dev.dtd m.met-dev.dtd file1.met-dev.dtd
127.0.0.1    www.sev-dev.dtd admin.sev-dev.dtd m.sev-dev.dtd file1.sev-dev.dtd
127.0.0.1    www.svj-dev.dtd admin.svj-dev.dtd m.svj-dev.dtd file1.svj-dev.dtd
127.0.0.1    www.geh-dev.dtd admin.geh-dev.dtd m.geh-dev.dtd file1.geh-dev.dtd
127.0.0.1    www.bb-dev.dtd admin.bb-dev.dtd m.bb-dev.dtd file1.bb-dev.dtd
127.0.0.1    www.vtl-dev.dtd admin.vtl-dev.dtd m.vtl-dev.dtd vtl.ts-dev.dtd file1.vtl.ts-dev.dtd vital.ts-dev.dtd file1.vital.ts-dev.dtd
127.0.0.1    vital.ts-dev.dtd m.vital.ts-dev.dtd file1.vital.ts-dev.dtd
127.0.0.1    www.ssn-dev.dtd admin.ssn-dev.dtd m.ssn-dev.dtd amp.ssn-dev.dtd file1.ssn-dev.dtd
127.0.0.1    www.drg-dev.dtd admin.drg-dev.dtd m.drg-dev.dtd amp.drg-dev.dtd file1.drg-dev.dtd
127.0.0.1    www.vdc-dev.dtd admin.vdc-dev.dtd m.vdc-dev.dtd amp.vdc-dev.dtd file1.vdc-dev.dtd
127.0.0.1    file1.gz-dev.dtd file1.cs-dev.dtd file1.ts-dev.dtd file1.tls-dev.dtd file1.pv-dev.dtd file1.rp-dev.dtd file1.standard-dev.dtd
127.0.0.1    file1.ea-dev.dtd file1.ssn-dev.dtd file1.drg-dev.dtd file1.bb-dev.dtd file1.dps-dev.dtd file1.inf-dev.dtd file1.met-dev.dtd
127.0.0.1    file1.vtl-dev.dtd file1.tls-dev.dtd file1.gtv-dev.dtd
127.0.0.1    www.standard-int.dtd admin.standard-int.dtd
127.0.0.1    actus.cs-int.dtd www.cs-int.dtd admin.cs-int.dtd m.cs-int.dtd
127.0.0.1    www.gz-int.dtd admin.gz-int.dtd m.gz-int.dtd
127.0.0.1    www.ts-int.dtd admin.ts-int.dtd m.ts-int.dtd
127.0.0.1    www.tls-int.dtd admin.tls-int.dtd m.tls-int.dtd
127.0.0.1    www.gtv-int.dtd admin.gtv-int.dtd
127.0.0.1    www.pv-int.dtd admin.pv-int.dtd m.pv-int.dtd
127.0.0.1    www.met-int.dtd admin.met-int.dtd m.met-int.dtd file1.met-dev.dtd
127.0.0.1    www.sev-int.dtd admin.sev-int.dtd m.sev-int.dtd
127.0.0.1    www.svj-int.dtd admin.svj-int.dtd m.svj-int.dtd
127.0.0.1    www.geh-int.dtd admin.geh-int.dtd m.geh-int.dtd
127.0.0.1    www.bb-int.dtd admin.bb-int.dtd m.bb-int.dtd
127.0.0.1    www.vtl-int.dtd admin.vtl-int.dtd m.vtl-int.dtd
127.0.0.1    www.met-dev.dtd admin.met-dev.dtd
127.0.0.1    www.mcs-dev.dtd admin.mcs-dev.dtd
127.0.0.1    www.ea-dev.dtd admin.ea-dev.dtd
127.0.0.1    www.pv-dev.dtd admin.pv-dev.dtd
127.0.0.1    file1.gz-int.dtd file1.cs-int.dtd file1.ts-int.dtd file1.tls-int.dtd file1.pv-int.dtd file1.rp-int.dtd file1.standard-int.dtd
127.0.0.1    file1.ea-int.dtd file1.ssn-int.dtd file1.drg-int.dtd file1.bb-int.dtd file1.dps-int.dtd file1.inf-int.dtd file1.met-int.dtd
127.0.0.1    file1.vtl-int.dtd file1.tls-int.dtd file1.gtv-int.dtd
127.0.0.1    www.atp-dev.dtd admin.atp-dev.dtd m.atp-dev.dtd file1.atp-dev.dtd
```    

```bash
cd /projects/docker-ezlight/
docker-compose up -d
```
- vérifier que tout s'est bien passé en tapant

```bash
    docker ps -a 
```
qui devrait afficher 5 containers running 

 - ezlight-nginx
 - ezlight-php
 - ezlight-nvm-node-less
 - ezlight-mariadb
 - ezlight-redis

si certain ont un status "exit" alors il y a eu un problème

Sinon on peut continuer : 

Importer la base de donnée fournie avec le repo, celle de modes et travaux, pour cela
- dezipper la base

```bash
cd /projects/docker-ezlight/mariadb/migration;
unzip modesettravaux_2020-01-29.zip;
```
- entrer dans le container mariadb :

```bash
docker exec -it  ezlight-mariadb /bin/bash;
```
 - importer la base :
```bash
cd /projects/docker-ezlight/mariadb/migration;
 mysql -uroot -ppass modesettravaux <  modesettravaux_2020-01-29.sql;
```

 - pendant qu'on est dans le container mysql on peut changer le mot de pass admin du backoffice : 

```bash
mysql -uroot -ppass modesettravaux -e "UPDATE ezuser SET password_hash='publish', password_hash_type=5 WHERE login='admin';";
```

 sortir du container avec `exit;` + [Enter]


# Modification applicative

## Modifier les settings

  - Pour accéder à la base de données
  
  Editer le fichier : `/projects/repo-standard.dtd/application/settings/override/site.ini.append.php.dev`

```ini
[DatabaseSettings]
Server=ezlight-mariadb
Port=3306
User=root
Password=pass
SQLLog=disabled 
```


  - Pour ne pas avoir besoin d'images en local et afficher celles de prod
  
  Editer le fichier : `/projects/repo-standard.dtd/application/settings/override/aws.ini.append.php.dev`

```ini
[ImagesBucketSettings] 
CloudfrontBaseUrl[modesettravaux]=http://file1modesettravaux.digimondo.net
```

et commenter la ligne existante comme suit avec # en début de ligne:

```ini
#CloudfrontBaseUrl=https://d1mzm0npacvjji.cloudfront.net
```
 
  Editer le fichier : `/projects/repo-standard.dtd/application/settings/override/image.ini.append.php.dev`

```ini
[AliasSettings]
ImageAliasClass[modesettravaux]=MFAWSS3ImagesHandler
```
   
## Regenerer les autoload

Lancer ces lignes de commandes :

```bash
chmod -R 777 /projects/standard.dtd/autoload;
docker exec ezlight-php su -s /bin/bash -c "cd /projects/standard.dtd; php bin/php/ezpgenerateautoloads.php -k" www-data;
docker exec ezlight-php su -s /bin/bash -c "cd /projects/standard.dtd; php bin/php/ezpgenerateautoloads.php -o" www-data;
docker exec ezlight-php su -s /bin/bash -c "cd /projects/standard.dtd; php bin/php/ezpgenerateautoloads.php -e" www-data;
```

# Vidage des cache ezpublish

Autant que possible faire

```bash
rm -rf /projects/standard.dtd/var/cache/*;
rm -rf /projects/standard.dtd/var/modesettravaux/cache/*;
rm -rf /projects/standard.dtd/var/DFSVar/var/modesettravaux/cache/*;
``` 

# Enfin Visualisation

http://www.met-dev.dtd  (front)

http://admin.met-dev.dtd  (back) 