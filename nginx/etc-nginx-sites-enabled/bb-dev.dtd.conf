server {
    listen 80;
    listen [::]:80;

    server_name *.bb-dev.dtd;
    # server_name www.bb-dev.dtd;
    # server_name amp.bb-dev.dtd;
    # server_name ~${SiteHostNamePreprod};

    root /projects/standard.dtd/;
    index index.php;

    # PROD
    #error_log /var/log/nginx/bb-dev.dtd.error.log;
    # DEV
    error_log /var/log/nginx/bb-dev.dtd.error.log notice;
    access_log /var/log/nginx/bb-dev.dtd.access.log;

    # POST des images
    client_max_body_size 100M;

    set $request_url $request_uri;

    location / {

        #CORS for nginx
        if ($request_url ~ \.(svg|ttf|ttc|otf|eot|woff|woff2|font.css|css)) {
            add_header Access-Control-Allow-Origin *;
        }

        #include /projects/standard.dtd/bb-dev.dtd.*nginx.rules;
        include /projects/repo-standard.dtd/application/root//.nginx.rules.dev*;

        #rewrite url with end slash
        rewrite '^(.+)/$' '$1';

        #rewrite ezfsfilehandler (default)
        rewrite '^/var/([^/]+)/storage/(.*)$' '/var/$1/storage/$2' break;
        rewrite '^/var/([^/]+)/cache/public/(.*)$' '/var/$1/cache/public/$2' break;

        #rewrite ezdfsfilehandler (uncomment if needed and comment previous lines)
        #rewrite '^/var/([^/]+)/storage/(.*)$' '/index_cluster.php' last;
        #rewrite '^/var/([^/]+)/cache/public/(.*)$' '/index_cluster.php' last;

        #design file with direct access
        rewrite '^/design/([^/]+)/(stylesheets|flash|fonts|images|lib|javascripts?)/(.*)' '/design/$1/$2/$3' break;
        rewrite '^/extension/([^/]+)/design/([^/]+)/(libraries|stylesheets|flash|fonts|images|lib|javascripts?)/(.*)' '/extension/$1/design/$2/$3/$4' break;
        rewrite '^/share/icons/(.*)' '/share/icons/$1' break;

        #rewrite treemenu
        rewrite '^/content/treemenu/?' '/index_treemenu.php' last;
        rewrite '^/tags/treemenu/?' '/index_treemenu_tags.php' last;

        if ( $request_uri ~ ^.*\/\(tous_les\).*$ ){
            return 410;
        }
        if ( $request_uri ~ ^\/\(page\)\/.*$ ){
            return 410;
        }
        if ( $request_uri ~ \/\(page\)(\/?$|\/[^\d]+|\/\d+(?!$)[^\d\/?#]) ){
            return 410;
        }

        rewrite '^(.*)$' '/index.php' last;
    }

    location ~ \.php$ {

        #CORS for nginx
        if ($request_url ~ \.(svg|ttf|ttc|otf|eot|woff|woff2|font.css|css)) {
            add_header Access-Control-Allow-Origin *;
        }

        fastcgi_pass php-upstream;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;

        fastcgi_param  PATH_INFO          $fastcgi_path_info;
        fastcgi_param  PATH_TRANSLATED    $document_root$fastcgi_path_info;
        fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
        fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
        fastcgi_param  REQUEST_URI        $request_url;
        fastcgi_index index.php;
    }
}
